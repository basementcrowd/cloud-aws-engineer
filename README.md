# BasementCrowd - Platform Engineer - AWS exercise #

This is a technical exercise for the platform engineering role in Basement Crowd. 

The CloudFormation template in this repository should create a new AWS stack. When visiting the URL in the result section, we would expect to view a page containing the text "Hello BasementCrowd". 

However the stack is failing. We would like you to:

* Fix the stack so that it works.
* Suggest some improvements to the existing stack to improve security, reliability, and scalability.


### Notes: ###
* ***Please, be aware that although the instances should be included in a new AWS account free tier, you might incur some costs. We recommend you configure a billing alert to avoid incurring charges, and be sure to delete the stack.***
* ***To avoid leaving the stack running, you are not required to share the URL with us to check - we can verify the stack by running it ourselves.***
* We are not expecting to have the perfect solution. But you should also be prepared to discuss the limitations and shortcuts.
* We understand that as an engineer you will be getting inspiration from multiple sources. We’re fine with that, but we wouldn’t like to get something largely copied from an existing project. We want to have a project from you.
* We understand some people may want to publish the solution as a showcase, but we ask you to refrain from including this description in the solution and from sharing this assignment with other people. 